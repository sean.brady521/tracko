import { NavigationContainer  } from '@react-navigation/native';
import { createBottomTabNavigator, useBottomTabBarHeight  } from '@react-navigation/bottom-tabs'
import React, { useEffect, useState } from 'react'
import * as Font from 'expo-font'
import { Ionicons } from '@expo/vector-icons'
import AppLoading from 'expo-app-loading'
import TrackScreen  from 'screens/track'
import ReportScreen  from 'screens/report'
import SettingsScreen  from 'screens/settings'
import { Icon } from 'native-base';
import colors from 'assets/theme/colors'

export type BottomTabParamList = {
  Track: undefined
  Report: undefined
  Settings: undefined
}

const bottomTabIcons = {
  Track: 'cash',
  Report: 'analytics',
  Settings: 'cog'
}

const BottomTab = createBottomTabNavigator<BottomTabParamList>()

export default function App() {
  const [isReady, setIsReady] = useState(false)


  useEffect(() => {
    Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    }).then(() => { 
      setIsReady(true)
    })
  })

  if (!isReady) {
    return <AppLoading />
  }

  return (
    <NavigationContainer>
      <BottomTab.Navigator 
        initialRouteName='Track'
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused }) => (
            <Icon 
              name={bottomTabIcons[route.name]} 
              style={{ 
                color: focused ? colors.primary : colors.blueyGrey,
              }} 
            />
          ),
        })}
        tabBarOptions={{
          activeTintColor: colors.primary,
          inactiveTintColor: colors.blueyGrey,
          tabStyle: { paddingBottom: 8, paddingTop: 8 },
          style: { height: 60 }
        }}
      >
        <BottomTab.Screen 
          name='Track'
          component={TrackScreen} 
        />
        <BottomTab.Screen 
          name='Report'
          component={ReportScreen} 
        />
        <BottomTab.Screen 
          name='Settings'
          component={SettingsScreen} 
        />
      </BottomTab.Navigator>
    </NavigationContainer>
  );
}