import { Container, Text, View } from 'native-base'
import React from 'react'
import styles from './transaction-item-styles'

interface TransactionItemSectionProps {
  description?: string
  style?: any
  amount: number
}

const TransactionItemSection = (props: TransactionItemSectionProps) => {
  const { description = 'No description', amount, style } = props
  return (
    <View style={{ ...styles.root, ...style }}>
      <Text>
        {description}
      </Text>
      <Text style={styles.amount}>
        ${amount.toFixed(2)}
      </Text>
    </View>
  )
}

export default TransactionItemSection