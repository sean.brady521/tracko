import { StyleSheet } from 'react-native'
import colors from 'assets/theme/colors'

export default StyleSheet.create({
  root: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  amount: {
    fontWeight: 'bold'
  }
})