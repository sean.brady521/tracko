import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  root: {
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  name: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  icon: {
    marginRight: 8
  }
})
