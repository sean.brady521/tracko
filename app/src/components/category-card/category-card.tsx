import { Button, Card, CardItem, Container, Icon, Text, View } from 'native-base'
import React from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler'
import styles from './category-card-styles'

interface CategoryCardProps {
  icon?: string
  name: string
  amount: number
  style?: object
  onPress: () => void
}

const CategoryCard = (props: CategoryCardProps) => {
  const {
    icon = 'basket',
    name,
    amount,
    style,
    onPress
  } = props

  return (
     <TouchableOpacity onPress={onPress}>
      <Card style={{...styles.root, ...style}}>
        <CardItem>
          <View style={styles.name}>
            <Icon style={styles.icon} name={icon} />
            <Text>
              {name}
            </Text>
          </View>
        </CardItem>
        <CardItem>
          <Text>
            ${amount}
          </Text>
        </CardItem>
      </Card>  
    </TouchableOpacity>
  )
}

export default CategoryCard