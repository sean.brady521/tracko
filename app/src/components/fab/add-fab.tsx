import { Button, Card, CardItem, Container, Icon, Text, View } from 'native-base'
import React from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler'
import styles from './add-fab-styles'

interface AddFabProps {
  text?: string
  full?: boolean
  onPress: () => void
}

const AddFab = (props: AddFabProps) => {
  const {
    text,
    full,
    onPress
  } = props

  const showFull = !!(text && full)

  return (
    <Button 
      rounded 
      style={{ ...styles.button, width: showFull ? 150 : 60 }}
      onPress={onPress}
    >
      <Icon name='add' style={styles.icon} />
      {showFull && 
        <Text style={styles.text}> 
          {text}
        </Text>
      }
    </Button>
  )
}

export default AddFab