import { StyleSheet } from 'react-native'
import colors from 'assets/theme/colors'

export default StyleSheet.create({
  button: {
    width: 60,
    height: 60,
    position: 'absolute',
    bottom: 16,
    right: 16,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.primary
  },
  icon: {
    fontSize: 32,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  text: {
    textTransform: 'capitalize',
  }
})
