
import { StyleSheet } from 'react-native'
import colors from 'assets/theme/colors'

export default StyleSheet.create({
  header: {
    marginTop: 45,
    backgroundColor: 'white'
  },
  backButtonWrap: {
    width: 150,
    alignItems: 'center',
    color: colors.blueyGrey
  },
  backIcon: {
    color: colors.blueyGrey
  },
  backText: {
    color: colors.blueyGrey
  },
})