import React from 'react'
import { Body, Button, Header, Icon, Left, Right, Text } from 'native-base'
import styles from './custom-header-styles' 

interface CustomHeaderProps {
  previous?: any,
  navigation?: any
}

const CustomHeader = (props: CustomHeaderProps) => {
  const { previous, navigation } = props
  return (
    <Header style={styles.header}>
      <Left>
        <Button transparent style={styles.backButtonWrap}>
          <Icon name='arrow-back' style={styles.backIcon} onPress={navigation.goBack} />
          <Text style={styles.backText}>
            Groceries
          </Text>
        </Button>
      </Left>
      <Body />
      <Right />
    </Header>
  )
}

export default CustomHeader