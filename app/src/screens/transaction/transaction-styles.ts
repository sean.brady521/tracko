import { StyleSheet } from 'react-native'
import colors from 'assets/theme/colors'

export default StyleSheet.create({
  root: {
    padding: 16,
    paddingTop: 80,
  },
})
