import { Container, Header, Icon, Text, View } from 'native-base'
import React from 'react'
import styles from './transaction-styles'
import { StackNavigationProp } from '@react-navigation/stack'
import { TrackStackParamList } from 'screens/track/track-stack'
import TransactionItem from 'components/transaction-item'

type TransactionNavigationProp = StackNavigationProp<TrackStackParamList, 'Transaction'>

export interface TransactionProps {
  navigation: TransactionNavigationProp
}

const Transaction = (props: TransactionProps) => {
  return (
    <Container style={styles.root}>
      <Text>
        transaction
      </Text>
    </Container>
  )
}

export default Transaction