import { BottomTabNavigationProp } from '@react-navigation/bottom-tabs'
import { Container, Text } from 'native-base'
import React from 'react'
import styles from './report-styles'
import { BottomTabParamList } from '../../../App'

type ReportNavigationProp = BottomTabNavigationProp<BottomTabParamList, 'Report'>

export interface ReportProps {
  navigation: ReportNavigationProp
}

const Report = (props: ReportProps) => {
  return (
    <Container style={styles.container}>
      <Text>
        Report
      </Text>
    </Container>
  )
}

export default Report