import { Button, Container, Fab, Header, Icon, Text, View } from 'native-base'
import React from 'react'
import styles from './track-styles'
import CategoryCard from 'components/category-card'
import { StackNavigationProp } from '@react-navigation/stack'
import { TrackStackParamList } from './track-stack'

type TrackNavigationProp = StackNavigationProp<TrackStackParamList, 'Track'>

export interface TrackProps {
  navigation: TrackNavigationProp
}

const Track = (props: TrackProps) => {
  const { navigation } = props
  return (
    <Container style={styles.root}>
      <Header transparent style={styles.headerSection}>
        <View>
          <Text style={styles.header}>
            Categories
          </Text>
          <Text style={styles.subHeader}>
            Monthly spending
          </Text>
        </View>
        <Button transparent>
          <Text style={styles.addButton}>
            Add
          </Text>
        </Button>
      </Header>
      <View style={styles.categories}>
        <CategoryCard style={styles.card} name='Groceries' amount={423} onPress={() => navigation.navigate('CategoryTransactions')} icon='cart' />
        <CategoryCard style={styles.card} name='Coffee' amount={124} onPress={() => console.log('click')} icon='cafe' />
        <CategoryCard style={styles.card} name='Health' amount={300} onPress={() => console.log('click')} icon='fitness' />
        <CategoryCard style={styles.card} name='Travel' amount={74} onPress={() => console.log('click')} icon='car' />
        <CategoryCard style={styles.card} name='Save to Spend' amount={620} onPress={() => console.log('click')}  />
        <CategoryCard style={styles.card} name='Uncategorised' amount={423} onPress={() => console.log('click')} icon='help' />
      </View>
      <Fab 
        onPress={() => {
          navigation.navigate('Transaction' as any)
        }} 
        position='bottomRight'
        style={styles.fab}
      >
        <Icon name='add' style={styles.fabIcon} />
      </Fab>

    </Container>
  )
}

export default Track