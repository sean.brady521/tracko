import { StyleSheet } from 'react-native'
import colors from 'assets/theme/colors'

export default StyleSheet.create({
  root: {
    padding: 16,
  },
  categories: {
    // display: 'block'
  },
  headerSection: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 24,
    marginBottom: 32 
  },
  header: {
    fontWeight: 'bold',
    fontSize: 18 
  },
  subHeader: {
    marginLeft: 24,
    color: colors.blueyGrey
  },
  addButton: {
    color: colors.primary,
    textTransform: 'capitalize'
  },
  fab: {
    backgroundColor: colors.primary,
  },
  fabIcon: {
    fontSize: 30 
  },
  card: {
    marginBottom: 8
  }
})
