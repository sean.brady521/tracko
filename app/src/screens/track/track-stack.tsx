import React from 'react'
import TransactionScreen from 'screens/transaction'
import CategoryTransactionsScreen from 'screens/category-transactions'
import TrackScreen  from './track'
import { createStackNavigator } from '@react-navigation/stack'

export type TrackStackParamList = {
  Transaction: undefined
  Track: undefined
  CategoryTransactions: undefined
}

const Stack = createStackNavigator<TrackStackParamList>();

const TrackStack = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name='Track' component={TrackScreen} />
      <Stack.Screen name='Transaction' component={TransactionScreen} />
      <Stack.Screen name='CategoryTransactions' component={CategoryTransactionsScreen} />
    </Stack.Navigator>
  ) 
}

export default TrackStack