import { BottomTabNavigationProp } from '@react-navigation/bottom-tabs'
import { Container, Text } from 'native-base'
import React from 'react'
import styles from './settings-styles'
import { BottomTabParamList } from '../../../App'

type SettingsNavigationProp = BottomTabNavigationProp<BottomTabParamList, 'Settings'>

export interface SettingsProps {
  navigation: SettingsNavigationProp
}

const Settings = (props: SettingsProps) => {
  return (
    <Container style={styles.container}>
      <Text>
        Settings
      </Text>
    </Container>
  )
}

export default Settings