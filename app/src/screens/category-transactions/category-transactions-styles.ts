import { StyleSheet } from 'react-native'
import colors from 'assets/theme/colors'

export default StyleSheet.create({
  root: {
  },
  contentWrap: {
    padding: 16,
  },
  header: {
    marginTop: 45,
    backgroundColor: 'white',
    alignItems: 'center'
  },
  backButtonWrap: {
    width: 150,
    alignItems: 'center',
    color: colors.blueyGrey
  },
  backIcon: {
    color: colors.blueyGrey
  },
  backText: {
    color: colors.blueyGrey
  },
  searchWrap: {
    backgroundColor: colors.highlightGrey,
    width: '100%',
    marginBottom: 16,
    borderRadius: 5
  },
  searchItem: {
    borderBottomWidth: 0
  },
  titleWrap: {
    width: '100%'
  },
  date: {
    width: '100%',
    color: colors.blueyGrey,
    fontSize: 12,
    padding: 8
  },
  dateWrap: {
    backgroundColor: colors.highlightGrey,
    borderRadius: 5,
    marginBottom: 16
  },
  item: {
    marginBottom: 16 
  },
  tranGroup: {
    paddingLeft: 8,
    paddingRight: 8,

  },
})
