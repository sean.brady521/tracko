import { Body, Button, Container, Header, Icon, Input, Item, Left, Right, Text, Title, View } from 'native-base'
import { StatusBar } from 'react-native'
import React from 'react'
import styles from './category-transactions-styles'
import { StackNavigationProp } from '@react-navigation/stack'
import { TrackStackParamList } from 'screens/track/track-stack'
import TransactionItem from 'components/transaction-item'


type CategoryTransactionsNavigationProp = StackNavigationProp<TrackStackParamList, 'CategoryTransactions'>

export interface CategoryTransactionsProps {
  navigation: CategoryTransactionsNavigationProp
}

const dummyData = {
  '29 JAN': [
    { id: '1', description: 'Cafe + Beans', amount: 18.00, timestamp: 1611878400 },
    { id: '2', description: 'Eggs', amount: 2.50, timestamp: 1612224000 },
  ],
  '15 JAN': [
    { id: '3', description: 'Bread', amount: 7.50, timestamp: 1609632000 },
  ],
  '2 JAN': [
    { id: '4', description: 'Body Wash', amount: 26.00, timestamp: 1609459200 },
  ]
}

const CategoryTransactions = (props: CategoryTransactionsProps) => {
  const { navigation } = props
  return (
    <Container style={styles.root}>
      <Header style={styles.header} >
        <Left>
          <Button transparent style={styles.backButtonWrap} onPress={navigation.goBack}>
            <Icon name='arrow-back' style={styles.backIcon}/>
            <Text style={styles.backText}>
              Groceries
            </Text>
          </Button>
        </Left>
        <Body />
        <Right />
      </Header>
      <View style={styles.contentWrap}>
        <View style={styles.searchWrap}>
          <Item style={styles.searchItem} underline={false}>
            <Icon name='search' />
            <Input placeholder='Search'/>
          </Item>
        </View>
        {Object.entries(dummyData).map(([date, transactions]) => (
          <View key={date}>
            <View style={styles.dateWrap}>
              <Text style={styles.date}>
                {date}
              </Text>
            </View>
            <View style={styles.tranGroup}>
              {transactions.map(tran => (
                <TransactionItem 
                  key={tran.id} 
                  description={tran.description} 
                  amount={tran.amount} 
                  style={styles.item}
                />
              ))}
            </View>
          </View>
        ))}
      </View>
    </Container>
  )
}

export default CategoryTransactions