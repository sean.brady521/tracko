const colors = {
  primary: 'hsl(143, 48%, 43%)',
  blueyGrey: 'hsl(245, 14%, 48%)',
  highlightGrey: 'hsl(245, 6%, 95%)',
}

export default colors