import { Response, NextFunction } from "express"
import admin from 'firebase-admin'
import { VerifiedRequest } from "../types/common"

export default async function verifyToken (req: VerifiedRequest, res: Response, next: NextFunction) {
  const token = req.headers.authorization

  if (!token) { 
    res.status(403).send({ auth: false, message: 'No token provided.' }) 
    return
  }

  let decodedToken
  try {
    decodedToken = await admin.auth().verifyIdToken(token)
  } catch (e) {
    res.status(500).send({ auth: false, message: e.message })
    return
  }

  req.userId = decodedToken.uid
  next()

}