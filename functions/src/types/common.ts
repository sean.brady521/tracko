import { Request } from "express";

export interface VerifiedRequest extends Request {
  userId?: string
}

export interface Transaction {
  id: string
  createdBy: string
  timestamp: number
  amount: number
  description: string
  category: string
}