import admin from 'firebase-admin'
import { Response, Express } from 'express'
import { Transaction, VerifiedRequest } from '../types/common'
import verifyToken from '../auth/VerifyTokens' 

export default (app: Express) => {
  app.get('/api/transactions', verifyToken, async (req: VerifiedRequest, res: Response) => {
    const db = admin.firestore()
    const transactionMatches = await db.collection('transactions').where('createdBy', '==', req.userId).get()

    const transactions: Transaction[] = []
    if (!transactionMatches.empty) {
      transactionMatches.forEach(transaction => { transactions.push(transaction.data() as Transaction) })
    }

    res.status(200).send(transactions)
  })

  app.post('/api/transaction', verifyToken, async (req: VerifiedRequest, res: Response) => {
    const db = admin.firestore()

    console.log(`transaction with id ${req.body.id}`)
    const docRef = db.collection('transactions').doc(req.body.id)
    const existingTran = await docRef.get()

    // Make sure we're not overwriting someone elses Transaction
    if (existingTran.exists) {
      const existingTranData = existingTran.data() as Transaction
      if (existingTranData.createdBy !== req.userId) {
        res.status(403).send('Cant go editting another guys Transaction, bit rude')
        return
      }
    }

    await docRef.set(req.body)

    res.status(200).send({ id: req.body.id })
  })

  app.delete('/api/transaction', verifyToken, async (req: VerifiedRequest, res: Response) => {
    const { userId, body } = req

    const db = admin.firestore()
    const docRef = db.collection('transactions').doc(body.id)

    const transaction = await docRef.get()
    // Make sure this user has permission to delete this Transaction
    if (transaction.exists) {
      const existingTran = transaction.data() as Transaction
      if (existingTran.createdBy !== userId) {
        res.status(403).send('That aint ya Transaction')
      } else {
        await docRef.delete()
        res.status(200).send({ id: body.id })
      }
    } else {
      res.status(404).send('Transaction does not exist')
    }
  })
}
